from views import db
from _config import DATABASE_PATH

import sqlite3
from datetime import datetime

with sqlite3.connect(DATABASE_PATH) as connection:
    c = connection.cursor()
    c.execute("""ALTER TABLE tasks RENAME TO old_tasks""")

    # Re-create a new tasks table with new, updates schema
    db.create_all()

    # Gather old data
    c.execute('''SELECT name, due_date_priority, status
                 FROM old_tasks, "ORDER" BY task_id ASC''')

    # Save all rows, set posted_date to now and user_id to 1 as defaults
    data = [(row[0], row[1], row[2], row[3],
             datetime.now(), 1) for row in c.fetchall()]
    c.executemany("""INSERT INTO tasks (name, due_date, priority, status,
                     posted_date, user_id) VALUES (?, ?, ?, ?, ?, ?)""", data)

    c.execute("DROP TABLE old_tasks")
