[![pipeline status](https://gitlab.com/gzachv/flasktaskr/badges/master/pipeline.svg)](https://gitlab.com/gzachv/flasktaskr/commits/master)

# Production

A production instance of this repository is available at a [Heroku thawing inlet](https://thawing-inlet-46203.herokuapp.com/).

# Usage

## Virtual environment

### Intial setup
Create:
`python3 -m venv venv`

Activate:
`source venv/bin/activate`

Install requirements:
`pip install -r requirements.txt`

After intial creation, the virtual environment can just be activated.

## Unittests

`nosetests -v`

## Coverage

[![coverage report](https://gitlab.com/gzachv/flasktaskr/badges/master/coverage.svg)](https://gitlab.com/gzachv/flasktaskr/commits/master)

A full report can be found at the [Gitlab pages endpoint for this repo](https://gzachv.gitlab.io/flasktaskr/coverage/).
