import os

# grab the folder where this script lives
basedir = os.path.abspath(os.path.dirname(__file__))

DATABASE = 'flasktaskr.db'
WTF_CSRF_ENABLED = True
SECRET_KEY = "K,E\xe3!\xfd\x82\xa3\x12)\xf8\xa9\x01\xcf\xac[\xe3\r\xafnp'R\x8b"

# define the full path for the database
DATABASE_PATH = os.path.join(basedir, DATABASE)

# SQLAlchemy settings
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Database URI
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + DATABASE_PATH

DEBUG = False
