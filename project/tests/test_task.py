import unittest

from test_helpers import TaskrTestFixture
from project.models import Task

class TaskTest(TaskrTestFixture):

    ############################
    #### setup and teardown ####
    ############################

    def setUp(self):
        super(TaskTest, self).setUp()

    def tearDown(self):
        super(TaskTest, self).tearDown()

    ############################
    ####     Task Tests     ####
    ############################

    def test_logged_in_users_can_access_tasks(self):
        self.assertIn(b'A', 'A')

    def test_logged_in_users_can_access_tasks(self):
        self.register('zvargas', 'zvargas@zvargas.com',
                                 'python101', 'python101')
        self.login('zvargas', 'python101')
        response = self.app.get('tasks/')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Add a new task:', response.data)

    def test_task_template_displays_logged_in_username(self):
        self.register('zvargas', 'zvargas@zvargas.com',
                                 'python101', 'python101')
        self.login('zvargas', 'python101')
        response = self.app.get('tasks/', follow_redirects=True)
        self.assertIn(b'zvargas', response.data)

    def test_not_logged_in_users_cannot_access_tasks(self):
        response = self.app.get('tasks/', follow_redirects=True)
        self.assertIn(b'You need to login', response.data)

    def test_users_can_add_tasks(self):
        self.create_user('zvargas', 'zvargas@zvargas.com', 'python101')
        self.login('zvargas', 'python101')
        self.app.get('tasks/', follow_redirects=True)
        response = self.create_task()
        self.assertIn(b'New entry was successfully posted.', response.data)

    def test_users_cannot_see_task_modify_link_for_others_tasks(self):
        self.create_user('zvargas', 'zvargas@zvargas.com', 'python101')
        self.login('zvargas', 'python101')
        self.app.get('tasks/', follow_redirects=True)
        self.create_task()
        self.logout()
        self.create_user('Michael', 'michael@realpython.com', 'python')
        response = self.login('Michael', 'python')
        self.app.get('tasks/', follow_redirects=True)
        self.assertNotIn(b'Delete', response.data)

    def test_users_can_see_task_modify_link_for_their_tasks(self):
        self.create_user('zvargas', 'zvargas@zvargas.com', 'python101')
        self.login('zvargas', 'python101')
        self.app.get('tasks/', follow_redirects=True)
        self.create_task()
        self.logout()
        self.create_user('Michael', 'michael@realpython.com', 'python')
        self.login('Michael', 'python')
        self.app.get('tasks/', follow_redirects=True)
        response = self.create_task()
        self.assertIn(b'complete/2/', response.data)
        self.assertIn(b'complete/2/', response.data)

    def test_admin_users_can_see_task_modify_links_for_all_tasks(self):
        self.register('Michael', 'michael@realpython.com', 'python', 'python')
        self.login('Michael', 'python')
        self.app.get('tasks/', follow_redirects=True)
        self.create_task()
        self.logout()
        self.create_admin_user('Superman', 'superman@admin.com', 'allpowerful')
        self.login('Superman', 'allpowerful')
        self.app.get('tasks/', follow_redirects=True)
        response = self.create_task()
        self.assertIn(b'complete/1/', response.data)
        self.assertIn(b'delete/1/', response.data)
        self.assertIn(b'complete/2/', response.data)
        self.assertIn(b'delete/2/', response.data)

    def test_users_cannot_add_tasks_when_error(self):
        self.create_user('zvargas', 'zvargas@zvargas.com', 'python101')
        self.login('zvargas', 'python101')
        self.app.get('tasks/', follow_redirects=True)
        response = self.app.post('add/', data=dict(name='Go to the bank',
                                                   due_date='',
                                                   posted_date='02/05/2017',
                                                   status=Task.OPEN_STATUS),
                                         follow_redirects=True)
        self.assertIn(b'This field is required.', response.data)

    def test_users_can_complete_tasks(self):
        self.create_user('zvargas', 'zvargas@zvargas.com', 'python101')
        self.login('zvargas', 'python101')
        self.app.get('tasks/', follow_redirects=True)
        self.create_task()
        response = self.app.get('complete/1/', follow_redirects=True)
        self.assertIn(b'The task was marked as completed', response.data)

    def test_users_can_delete_tasks(self):
        self.create_user('zvargas', 'zvargas@zvargas.com', 'python101')
        self.login('zvargas', 'python101')
        self.app.get('tasks/', follow_redirects=True)
        self.create_task()
        response = self.app.get('delete/1/', follow_redirects=True)
        self.assertIn(b'The task was deleted', response.data)

    def test_users_cannot_complete_others_tasks(self):
        self.create_user('zvargas', 'zvargas@zvargas.com', 'python101')
        self.login('zvargas', 'python101')
        self.app.get('tasks/', follow_redirects=True)
        self.create_task()
        self.logout()
        self.create_user('msimonson', 'msimonson@zvargas.com', 'python101')
        self.login('msimonson', 'python101')
        self.app.get('tasks/', follow_redirects=True)
        response = self.app.get('complete/1/', follow_redirects=True)
        self.assertNotIn(b'The task was marked as completed', response.data)
        self.assertIn(b'You can only complete tasks that belong to you.',
                      response.data)

    def test_users_cannot_delete_tasks_that_are_not_created_by_them(self):
        self.create_user('Michael', 'michael@realpython.com', 'python')
        self.login('Michael', 'python')
        self.app.get('tasks/', follow_redirects=True)
        self.create_task()
        self.logout()
        self.create_user('Fletcher', 'fletcher@realpython.com', 'python101')
        self.login('Fletcher', 'python101')
        self.app.get('tasks/', follow_redirects=True)
        response = self.app.get("delete/1/", follow_redirects=True)
        self.assertIn(b'You can only delete tasks that belong to you.',
                      response.data)

    def test_admin_users_can_complete_tasks_that_are_not_created_by_them(self):
        self.create_user('Michael', 'michael@realpython.com', 'python')
        self.login('Michael', 'python')
        self.app.get('tasks/', follow_redirects=True)
        self.create_task()
        self.logout()
        self.create_admin_user('Superman', 'superman@admin.com', 'allpowerful')
        self.login('Superman', 'allpowerful')
        self.app.get('tasks/', follow_redirects=True)
        response = self.app.get("complete/1/", follow_redirects=True)
        self.assertNotIn(b'You can only complete tasks that belong to you.',
                         response.data)

    def test_admin_users_can_delete_tasks_that_are_not_created_by_them(self):
        self.create_user('Michael', 'michael@realpython.com', 'python')
        self.login('Michael', 'python')
        self.app.get('tasks/', follow_redirects=True)
        self.create_task()
        self.logout()
        self.create_admin_user('Superman', 'superman@admin.com', 'allpowerful')
        self.login('Superman', 'allpowerful')
        self.app.get('tasks/', follow_redirects=True)
        response = self.app.get("delete/1/", follow_redirects=True)
        self.assertNotIn(b'You can only delete tasks that belong to you.',
                         response.data)

if __name__ == "__main__":
    unittest.main()
