import os
import unittest

from test_helpers import TaskrTestFixture

from project import app, db
from project.models import User

class MainTests(TaskrTestFixture):

    ############################
    #### setup and teardown ####
    ############################

    def setUp(self):
        super(MainTests, self).setUp()

    def tearDown(self):
        super(MainTests, self).tearDown()

    ###############
    #### tests ####
    ###############

    def test_404_error(self):
        response = self.app.get('/this-route-does-not-exist/')
        self.assertEquals(response.status_code, 404)
        self.assertIn(b'Sorry, there is nothing here.', response.data)

    def test_500_error(self):
        bad_user = User(
            name='Jeremy',
            email='jeremy@realpython.com',
            password='django'
        )
        db.session.add(bad_user)
        db.session.commit()
        self.assertRaises(ValueError, self.login, 'Jeremy', 'django')
        try:
            response = self.login('Jeremy', 'django')
            self.assertEquals(response.status_code, 500)
        except ValueError:
            pass

if __name__ == "__main__":
    unittest.main()
