import unittest

from test_helpers import TaskrTestFixture
from project import db
from project.models import User

class UserTest(TaskrTestFixture):

    ############################
    #### setup and teardown ####
    ############################

    def setUp(self):
        super(UserTest, self).setUp()

    def tearDown(self):
        super(UserTest, self).tearDown()

    ############################
    ####       tests        ####
    ############################
    # NOTE: Each test must start with 'test' prefix

    def test_login_form_is_present(self):
        self.assertIn(b'A', 'A')

    def test_login_form_is_present(self):
        self.check_form('/', b'Please login to access your task list')

    # Login tests
    def test_users_cannot_login_unless_registered(self):
        response = self.login('foo', 'bar')
        self.assertIn(b'Invalid username or password.', response.data)

    def test_users_can_login(self):
        response = self.register('zvargas', 'zvargas@zvargas.com',
                                 'python', 'python')
        response = self.login('zvargas', 'python')
        self.assertIn(b'Welcome to FlaskTaskr', response.data)

    def test_invalid_form_data(self):
        response = self.register('zvargas', 'zvargas@zvargas.com',
                                 'python', 'python')
        response = self.login('foo', 'bar')
        self.assertIn(b'Invalid username or password.', response.data)

    # Register tests
    def test_register_form_is_present(self):
        self.check_form('register/',
                        b'Please register to access the task list')

    def test_user_registration(self):
        response = self.register('zvargas', 'zvargas@zvargas.com',
                                 'python', 'python')
        self.assertIn(b'Thank you for registering, please login.',
                      response.data)

    def test_user_registration_error(self):
        self.register('zvargas', 'zvargas@zvargas.com',
                                 'python', 'python')
        response = self.register('zvargas', 'zvargas@zvargas.com',
                                 'python', 'python')
        self.assertIn(b'That username and/or email already exists.',
                      response.data)

    # User tests
    def test_logged_in_users_can_logout(self):
        self.register('zvargas', 'zvargas@zvargas.com',
                                 'python101', 'python101')
        self.login('zvargas', 'python101')
        response = self.logout()
        self.assertIn(b'Bye Felicia', response.data)

    def test_not_logged_in_users_cannot_logout(self):
        response = self.logout()
        self.assertNotIn(b'Bye Felicia', response.data)

    def test_default_user_role(self):
        db.session.add(
            User(
                "Johnny",
                "john@doe.com",
                "johnny"
            )
        )
        db.session.commit()

        users = db.session.query(User).all()
        for user in users:
            self.assertEqual(user.role, 'user')

if __name__ == "__main__":
    unittest.main()
