import os

import unittest

from project import app, db, bcrypt
from project.models import Task, User
from project._config import basedir

TEST_DB = 'test.db'

class TaskrTestFixture(unittest.TestCase):

    ############################
    #### setup and teardown ####
    ############################

    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABSE_URI'] = 'sqlite:///' + \
                                                os.path.join(basedir, TEST_DB)
        self.app = app.test_client()
        db.create_all()

        self.assertEquals(app.debug, False)

    def tearDown(cls):
        db.session.remove()
        db.drop_all()

    ############################
    ####      helpers       ####
    ############################

    def login(self, name, password):
        return self.app.post('/', data=dict(name=name, password=password),
                follow_redirects=True)

    def register(self, name, email, password, confirm):
        return self.app.post('register/', data=dict(name=name, email=email,
                                                    password=password,
                                                    confirm=confirm),
                follow_redirects=True)

    def check_form(self, route, key_string):
        response = self.app.get(route)
        self.assertEqual(response.status_code, 200)
        self.assertIn(key_string, response.data)

    def logout(self):
        return self.app.get('logout/', follow_redirects=True)

    def create_user(self, name, email, password):
        new_user = User(name=name, email=email,
                        password=bcrypt.generate_password_hash(password))
        db.session.add(new_user)
        db.session.commit()

    def create_admin_user(self, name, email, password):
        new_user = User(name=name, email=email,
                        password=bcrypt.generate_password_hash(password),
                        role='admin')
        db.session.add(new_user)
        db.session.commit()

    def create_task(self):
        return self.app.post('add/', data=dict(name="Go to the bank",
                                               due_date='10/20/2019',
                                               priority='1',
                                               posted_date='10/20/2017',
                                               status=Task.OPEN_STATUS),
                             follow_redirects=True)

if __name__ == "__main__":
    unittest.main()
